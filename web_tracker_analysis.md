<h1>Catch: Web Tracker Data Analysis</h1>


Here we are using Postgres as our backend database.

1. Creating table for source of truth we called it `sot_raw`

   ```sql
   drop table if exists catch_sot_raw;
   create table if not exists catch_sot_raw (
   	date_id text,
   	order_id text,
   	customer_platform text,
   	payment_method text,
   	product_id text
   );
   ```

   I am loading the file using copy command from Postgres:

   ```shell
   \copy public.catch_sot_raw from '/path/to/file/source_of_truth_order_report.csv' delimiter ',' CSV HEADER;
   ```

   Here I found some issues when I tried to load the file is not loaded properly. I got some trouble while importing the file into table since there are some tricky text such as there 3 double quotes in certain value that makes import process failed for example: `“I do not have 5” monitor”`. So before doing import I open the source_of_truth file in spreadsheet and save again into csv file. To tackle this issue I think I have to remove the last column (product name) since in this case that column is not significant rather than `product_id`. To remove that I am doing small shell script command:

   ```shell
   awk -F "," '{print $1,"|",$2,"|",$3,"|",$4,"|",$5}' source_of_truth_order_report.csv > source_of_truth_order_report_new.csv;
   
   sed 's/ | /,/g' source_of_truth_order_report_new.csv > source_of_truth_order_report_new1.csv;
   
   mv source_of_truth_order_report_new1.csv source_of_truth_order_report_new.csv;
   ```

   That command will dismiss last column (`product_name`) since that column is not significant enough in current case. After that I can loaded all data into `sot_raw` table.

2. Doing transformation for `sot_raw` table to `sot_stg`

   Previously, I only use text as main data type to ensure all data is loaded. Next, we have to transform and adding some columns to proper type.

   ```sql
   drop table if exists catch_sot_stg;
   create table catch_sot_stg as
   select date_id::date, cast(nullif(order_id, '') as bigint) order_id, customer_platform,
           SPLIT_PART(customer_platform, '--', 1) as platform,
           SPLIT_PART(customer_platform, '--', 2) as platform_2,
              payment_method, cast(nullif(product_id, '') as bigint) product_id 
     from catch_sot_raw as csr;
   ```

   Some explanation related to extra columns:

   - `platform` is column that I will use as join key against web tracker table, the content of that column will be like "Web, Mobile, etc"
   - `platform_2` is column that show us kind of device (iPad, iPhone, etc), platform (Android, etc)

   - `nullif` function is need to use to prevent data type casting error since we can't convert blank string (`""`) to a number (`bigint` in this case)

3. Creating table for web tracker

   ```sql
   create table if not exists catch_wot_raw (
   	date_id text,
   	br_name text,
   	br_family text,
   	br_version text,
   	br_type text,
   	os_name text,
   	os_family text,
   	product_id text,
   	order_id text
   );
   ```

   There is no significant issue while loading the web tracker file into table so the loading pretty straight forward and we can continue to the transformation:

   ```SQL
   create table catch_wot_stg as
   select to_date(date_id, 'DD/MM/YYYY') date_id, br_name, br_family, br_version, br_type,
   		case when br_type = 'Browser (mobile)' then 'Mobile Web'
   			 when br_type = 'Browser' then 'Web'
   		else br_type end platform,
   		os_name, os_family, cast(nullif(product_id, '') as bigint) product_id, cast(nullif(order_id, '') as bigint) order_id
     from catch_wot_raw as cwr;
   ```

   Explanation:

   - `platform` is the column that contains almost same value as `platform` column on `sot_stg` table;
   - There is casting from string to date with custom format using `to_date` function so we can match it nor use it as join key against `sot_stg` table.

4. First analysis started to inspect is there any missing data from tracker nor the `SoT`.

   ```sql
   select css.product_id, css.order_id, css.date_id date_a, cws.date_id date_b, 
   		css.platform platform_a, cws.platform platform_b
     from catch_sot_stg as css
     left join catch_wot_stg as cws on css.order_id = cws.order_id and css.product_id = cws.product_id;
   ```

   From that query we can find the difference between `sot` and `wot` based on the platform that customer use to order something. To view only the difference we can fire up this query:

   ```sql
   create table catch_analytic_webtracker_stg1 as
   select css.order_id, css.product_id, css.date_id date_a, cws.date_id date_b, 
     css.platform platform_a, cws.platform platform_b
     from catch_sot_stg as css
     full join catch_wot_stg as cws on css.order_id = cws.order_id 
           and css.product_id = cws.product_id
           and css.date_id = cws.date_id
    where cws.date_id is null or cws.platform is null or cws.platform = ''
    or css.date_id != cws.date_id or css.platform != cws.platform;
   ```

   Query above is base query that list mismatched data with criteria:
   
   - Is there any difference between `wot` and `sot` related to date?

     To address this, based on `catch_analytic_webtracker_stg1` query we already answer that since we already use `date_id` as key join. So to answer this we just select with filter `date_a != date_b`.
   
     ```sql
     select * from catch_analytic_webtracker_stg1
      where date_a != date_b;
     ```
   
   - Is there any difference between platform that customer use?
   
     We can check this one by firing this query:
   
     ```sql
     select * from catch_analytic_webtracker_stg1
      where platform_a != platform_b;
     ```
   
     The common difference that comes up from query above as below:
   
     | order_id | product_id | date_a     | date_b     | platform_a   | platform_b   |
     | -------- | ---------- | ---------- | ---------- | ------------ | ------------ |
     | 27154709 | 2906532    | 2019-08-09 | 2019-08-09 | `App`        | `Mobile Web` |
     | 27188920 | 2784610    | 2019-08-12 | 2019-08-12 | `App`        | Web          |
     | 27185349 | 2256164    | 2019-08-11 | 2019-08-11 | `App`        | `Mobile Web` |
     | 27184508 | 4074121    | 2019-08-11 | 2019-08-11 | `App`        | `Mobile Web` |
     | 27090060 | 1228227    | 2019-08-04 | 2019-08-04 | `App`        | `Mobile Web` |
     | 27324689 | 3521434    | 2019-08-21 | 2019-08-21 | `Mobile Web` | `Web`        |
   
     The result show us the tracker <u>cannot</u> capture the customer while customer using `"App"` instead of that the web tracker show us <u>Mobile Web</u> but we cannot assume all `"App"` platform is `Mobile Web` since there are customer using `Mobile Web` and it detect `Web`.
   
   - Is there any data that exists in `sot` but not exists in `wot`?
   
     We can check this by this one:
   
     ```sql
     select * from catch_analytic_webtracker_stg1
      where date_b is null;
     ```
   
     That query proofs us that there are some data is not detected by web tracker.
   
5. Summary:

   - There are `31988 rows` data that not detected from web tracker means the accuracy of tracker is about `99%` accurate;

     ```sql
     select count(*) from (
     select * from catch_analytic_webtracker_stg1
      where date_b is null) x;
     ```

   - There are `163546 rows` data that mismatch between tracker means the accuracy of tracker related to detecting platform is about `20%`.

     ```sql
     select count(*) from (
     select * from catch_analytic_webtracker_stg1
      where platform_a != platform_b) x;
     ```

     